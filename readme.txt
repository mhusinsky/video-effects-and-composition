Video Effects and Compositing workshop material, Anton M.
http://node13.vvvv.org/program/video-effects-and-compositing/

First of all two FullscreenQuads are going to unc and lecloneur for their TextureFX nodes used in this workshop.

1) Please install the 'Tutor' font (by www.zetafonts.com). You can find the font under Assets\Font.

2) Keep in mind that everywhere in examples a video can be used as a texture instead of just a static image.

3) A note about FileStream (EX9.Texture VLC): If you start an example and a video is not playing (however a video file is chosen in the 'Filename' IOBox), try to create a Preview (EX9.Texture) and connect the 'Texture Output' of the FileStream (EX9.Texture VLC) to its 'Input'. The video should start and the Preview node can be deleted now.

-------------

And don't forget: low gear gives you power, high gear gives you speed.